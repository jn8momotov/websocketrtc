//
//  WebSocket.swift
//  WebSocketApp
//
//  Created by Евгений on 10/01/2019.
//  Copyright © 2019 Momotov. All rights reserved.
//

import Foundation
import Starscream
import WebRTC
import SwiftHash

protocol WebSocketSignalDelegate: class {
    func signalClientDidConnect(_ signalClient: WebSocketSignal)
    func signalClientDidDisconnect(_ signalClient: WebSocketSignal)
}

class WebSocketSignal {
    
    fileprivate let urlString = "ws://94.250.250.224:8088/ws"
    let socket: WebSocket
    
    weak var delegate: WebSocketSignalDelegate?
    
    init() {
        let url = URL(string: urlString)!
        socket = WebSocket(url: url, protocols: ["sip"])
    }
    
    func connect() {
        print("Connectiong...")
        socket.delegate = self
        socket.connect()
    }
    
    func calculateResponse(user: String, realm: String, password: String, method: String, uri: String, nonce: String) -> String {
        let h1 = MD5("\(user):\(realm):\(password)")
        let h2 = MD5("\(method):\(uri)")
        return MD5("\(h1):\(nonce):\(h2)")
    }
    
    var cSeq = 27272
    
    func send(nonce: String) {
        
        let username = "1559"
        let realm = "voip.iot.ru"
        let password = "EsdcBYxctIhFMUj"
        let method = "REGISTER"
        let uri = "sip:94.250.250.224"
        
        let secret = calculateResponse(user: username, realm: realm, password: password, method: method, uri: uri, nonce: nonce)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let callID = dateFormatter.string(from: Date())
        
        let message = """
REGISTER \(uri) SIP/2.0
Via: SIP/2.0/WS df7jal23ls0d.invalid;branch=z9hG4bKxZpzkT39TJ7xAnrXglvRfsVJCahmBbZx;rport
From: "den"<sip:\(username)@94.250.250.224>;tag=QortDVGDv8DeE5s3AQ5x
To: "den"<sip:\(username)@94.250.250.224>
Contact: "den"<sip:\(username)@df7jal23ls0d.invalid;rtcweb-breaker=no;transport=ws>;expires=200;click2call=no;+g.oma.sip-im;+audio;language="en,fr"
Call-ID: \(callID)
CSeq: \(cSeq) REGISTER
Content-Length: 0
Max-Forwards: 70
Authorization: Digest username="\(username)",realm="\(realm)",nonce="\(nonce)",uri="\(uri)",response="\(secret)",algorithm=MD5
User-Agent: IM-client/OMA1.0 sipML5-v1.2016.03.04
Organization: Doubango Telecom
Supported: path


"""
    
        print("-----Send------")
        print("\(message)")
        print("---------------")
        
        if let data = message.data(using: .utf8) {
            socket.write(data: data)
            cSeq += 1
        } else {
            print("Error get data from sdp message")
        }
    }
    
    func send() {
        socket.write(string: "SDP") {
            print("Send message: SDP")
        }
    }
    
}

extension WebSocketSignal: WebSocketDelegate {
    
    func websocketDidConnect(socket: WebSocketClient) {
        print("Connect to Server")
    }
    
    func websocketDidDisconnect(socket: WebSocketClient, error: Error?) {
        if let error = error {
            print("Did disconnect: \(error.localizedDescription)")
        } else {
            print("Did disconnect")
        }
        print("Repeat connecting...")
        socket.connect()
    }
    
    func websocketDidReceiveMessage(socket: WebSocketClient, text: String) {
        print("Receive message: \(text)")
    }
    
    func websocketDidReceiveData(socket: WebSocketClient, data: Data) {
        print("Receive data:")
    }
    
}
