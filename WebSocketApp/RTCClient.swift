//
//  RTCClient.swift
//  WebSocketApp
//
//  Created by Евгений on 14/01/2019.
//  Copyright © 2019 Momotov. All rights reserved.
//

import Foundation
import WebRTC

public enum RTCClientState {
    case disconnected
    case connecting
    case connected
}

public protocol RTCClientDelegate: class {
    func rtcClient(client : RTCClient, startCallWithSdp sdp: String)
    func rtcClient(client : RTCClient, didReceiveLocalVideoTrack localVideoTrack: RTCVideoTrack)
    func rtcClient(client : RTCClient, didReceiveRemoteVideoTrack remoteVideoTrack: RTCVideoTrack)
    func rtcClient(client : RTCClient, didReceiveError error: Error)
    func rtcClient(client : RTCClient, didChangeConnectionState connectionState: RTCIceConnectionState)
    func rtcClient(client : RTCClient, didChangeState state: RTCClientState)
    func rtcClient(client : RTCClient, didGenerateIceCandidate iceCandidate: RTCIceCandidate)
    func rtcClient(client : RTCClient, didCreateLocalCapturer capturer: RTCCameraVideoCapturer)
}

public extension RTCClientDelegate {
    
    func rtcClient(client : RTCClient, didReceiveError error: Error) {
        
    }
    
    func rtcClient(client : RTCClient, didChangeConnectionState connectionState: RTCIceConnectionState) {
        
    }
    
    func rtcClient(client : RTCClient, didChangeState state: RTCClientState) {
        
    }
}

public class RTCClient: NSObject {
    
    var delegate: RTCClientDelegate?
    
    var connectionFactory = RTCPeerConnectionFactory()
    var peerConnection: RTCPeerConnection?
    var iceServers: [RTCIceServer] = []
    var remoteIceCandidates: [RTCIceCandidate] = []
    
    let videoCallConstraint = RTCMediaConstraints(mandatoryConstraints: ["OfferToReceiveVideo": "true"], optionalConstraints: nil)
    let defaultConstraints = RTCMediaConstraints(mandatoryConstraints: nil, optionalConstraints: ["DtlsSrtpKeyAgreement": "true"])
    
    public override init() {
        super.init()
    }
    
    public convenience init(iceServers: [RTCIceServer]) {
        self.init()
        self.iceServers = iceServers
        self.configure()
    }
    
    func configure() {
        initPeerConnectionFactory()
        initPeerConnection()
    }
    
    func initPeerConnectionFactory() {
        RTCPeerConnectionFactory.initialize()
        connectionFactory = RTCPeerConnectionFactory()
        
    }
    
    func initPeerConnection() {
        let config = RTCConfiguration()
        config.iceServers = iceServers
        peerConnection = connectionFactory.peerConnection(with: config, constraints: defaultConstraints, delegate: self)
    }
    
    func startConnection() {
        guard let peerConnection = peerConnection else {
            print("Peer connection is nil!")
            return
        }
        let localStream = self.localStream()
        peerConnection.add(localStream)
        if let localVideoTrack = localStream.videoTracks.first {
            self.delegate?.rtcClient(client: self, didReceiveLocalVideoTrack: localVideoTrack)
        }
    }
    
    func localStream() -> RTCMediaStream {
        let factory = connectionFactory
        let localStream = factory.mediaStream(withStreamId: "RTCmS")
        let videoSource = factory.videoSource()
        let capturer = RTCCameraVideoCapturer(delegate: videoSource)
        self.delegate?.rtcClient(client: self, didCreateLocalCapturer: capturer)
        let videoTrack = factory.videoTrack(with: videoSource, trackId: "RTCvS0")
        videoTrack.isEnabled = true
        localStream.addVideoTrack(videoTrack)
        return localStream
    }
    
    func makeOffer() {
        guard let peerConnection = peerConnection else {
            print("Peer connection is nil!")
            return
        }
        peerConnection.offer(for: videoCallConstraint) { [weak self] (sdp, error) in
            guard let this = self else { return }
            if let error = error {
                print("Error create offer: \(error.localizedDescription)")
                this.delegate?.rtcClient(client: this, didReceiveError: error)
                return
            } else {
                this.handleSdpGenerated(sdp: sdp)
            }
            
        }
    }
    
    func handleAnswerReceived(withRemoteSDP remoteSdp: String?) {
        guard let remoteSdp = remoteSdp else {
            return
        }
        
        // Add remote description
        let sessionDescription = RTCSessionDescription.init(type: .answer, sdp: remoteSdp)
        self.peerConnection?.setRemoteDescription(sessionDescription, completionHandler: { [weak self] (error) in
            guard let this = self else { return }
            if let error = error {
                this.delegate?.rtcClient(client: this, didReceiveError: error)
            } else {
                this.handleRemoteDescriptionSet()
            }
        })
    }
    
    func createAnswerForOfferReceived(withRemoteSDP remoteSdp: String?) {
        guard let remoteSdp = remoteSdp, let peerConnection = self.peerConnection else {
                return
        }
        
        // Add remote description
        let sessionDescription = RTCSessionDescription(type: .offer, sdp: remoteSdp)
        self.peerConnection?.setRemoteDescription(sessionDescription, completionHandler: { [weak self] (error) in
            guard let this = self else { return }
            if let error = error {
                this.delegate?.rtcClient(client: this, didReceiveError: error)
            } else {
                this.handleRemoteDescriptionSet()
                // create answer
                peerConnection.answer(for: this.videoCallConstraint, completionHandler:
                    { (sdp, error) in
                        if let error = error {
                            this.delegate?.rtcClient(client: this, didReceiveError: error)
                        } else {
                            this.handleSdpGenerated(sdp: sdp)
                        }
                })
            }
        })
    }
    
    func handleSdpGenerated(sdp: RTCSessionDescription?) {
        guard let sdpDescription = sdp else {
            return
        }
        self.peerConnection?.setLocalDescription(sdpDescription, completionHandler: { [weak self] (error) in
            guard let this = self, let error = error else { return }
            this.delegate?.rtcClient(client: this, didReceiveError: error)
        })
        //  Signal to server to pass this sdp with for the session call
        self.delegate?.rtcClient(client: self, startCallWithSdp: sdpDescription.sdp)
    }
    
    func handleRemoteDescriptionSet() {
        for iceCandidate in self.remoteIceCandidates {
            self.peerConnection?.add(iceCandidate)
        }
        self.remoteIceCandidates = []
    }
    
    func addIceCandidate(iceCandidate: RTCIceCandidate) {
        // Set ice candidate after setting remote description
        if self.peerConnection?.remoteDescription != nil {
            self.peerConnection?.add(iceCandidate)
        } else {
            self.remoteIceCandidates.append(iceCandidate)
        }
    }
    
}

extension RTCClient: RTCPeerConnectionDelegate {
    
    public func peerConnection(_ peerConnection: RTCPeerConnection, didChange stateChanged: RTCSignalingState) {
        
    }
    
    public func peerConnection(_ peerConnection: RTCPeerConnection, didAdd stream: RTCMediaStream) {
        if stream.videoTracks.count > 0 {
            self.delegate?.rtcClient(client: self, didReceiveRemoteVideoTrack: stream.videoTracks[0])
        }
    }
    
    public func peerConnection(_ peerConnection: RTCPeerConnection, didRemove stream: RTCMediaStream) {
        
    }
    
    public func peerConnectionShouldNegotiate(_ peerConnection: RTCPeerConnection) {
        
    }
    
    public func peerConnection(_ peerConnection: RTCPeerConnection, didChange newState: RTCIceConnectionState) {
        self.delegate?.rtcClient(client: self, didChangeConnectionState: newState)
    }
    
    public func peerConnection(_ peerConnection: RTCPeerConnection, didChange newState: RTCIceGatheringState) {
        
    }
    
    public func peerConnection(_ peerConnection: RTCPeerConnection, didGenerate candidate: RTCIceCandidate) {
        self.delegate?.rtcClient(client: self, didGenerateIceCandidate: candidate)
    }
    
    public func peerConnection(_ peerConnection: RTCPeerConnection, didRemove candidates: [RTCIceCandidate]) {
        
    }
    
    public func peerConnection(_ peerConnection: RTCPeerConnection, didOpen dataChannel: RTCDataChannel) {
        
    }
    
}
