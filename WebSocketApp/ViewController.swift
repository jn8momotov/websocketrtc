//
//  ViewController.swift
//  WebSocketApp
//
//  Created by Евгений on 10/01/2019.
//  Copyright © 2019 Momotov. All rights reserved.
//

import UIKit
import AVFoundation
import Starscream

class ViewController: UIViewController {
    
    let socketSignal: WebSocketSignal = WebSocketSignal()
    
    var captureSession: AVCaptureSession!
    var stillImageOutput: AVCapturePhotoOutput!
    var videoPreviewLayer: AVCaptureVideoPreviewLayer!

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var videoView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        socketSignal.connect()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        captureSession.stopRunning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        captureSession = AVCaptureSession()
        captureSession.sessionPreset = .medium
        
        guard let backCamera = AVCaptureDevice.default(for: .video) else {
            print("Not access to back camera")
            return
        }
        
        do {
            let input = try AVCaptureDeviceInput(device: backCamera)
            stillImageOutput = AVCapturePhotoOutput()
            if captureSession.canAddInput(input) && captureSession.canAddOutput(stillImageOutput) {
                captureSession.addInput(input)
                captureSession.addOutput(stillImageOutput)
                setupLivePreview()
            }
        } catch let error {
            print("Error: \(error.localizedDescription)")
        }
    }
    
    @IBAction func capturePhotoButtonPressed(_ sender: UIButton) {
        print("Button pressed")
        //socketSignal.send()
    }
    
    func setupLivePreview() {
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        
        videoPreviewLayer.videoGravity = .resizeAspect
        videoPreviewLayer.connection?.videoOrientation = .portrait
        videoView.layer.addSublayer(videoPreviewLayer)
        
        DispatchQueue.global(qos: .userInitiated).async {
            self.captureSession.startRunning()
            DispatchQueue.main.async {
                self.videoPreviewLayer.frame = self.videoView.bounds
            }
        }
    }

}

extension ViewController: AVCapturePhotoCaptureDelegate {
    
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        print("Did finish processing photo!")
        if let error = error {
            print("Error: \(error.localizedDescription)")
            
        }
    
        let data = photo.fileDataRepresentation()
        if let data = data {
            print("Get data!")
            imageView.image = UIImage(data: data)
            //socketSignal.send(data: data)
        } else {
            print("Data is nil")
        }
    }
    
}

