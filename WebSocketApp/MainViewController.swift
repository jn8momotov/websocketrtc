//
//  MainViewController.swift
//  WebSocketApp
//
//  Created by Евгений on 14/01/2019.
//  Copyright © 2019 Momotov. All rights reserved.
//

import UIKit
import WebRTC

class MainViewController: UIViewController {
    
    var client: RTCClient!
    var socketSignal: WebSocketSignal!
    
    var localVideoTrack: RTCVideoTrack?
    var remoteVideoTrack: RTCVideoTrack?
    
    var captureController: RTCCapturer!
    
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var localVideoView: RTCEAGLVideoView!
    @IBOutlet weak var remoteVideoView: RTCEAGLVideoView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        socketSignal = WebSocketSignal()
        socketSignal.connect()
        
        let iceServer = RTCIceServer(urlStrings: ["stun:stun.l.google.com:19302"])
        let iceServere = RTCIceServer(urlStrings: ["stun:stun1.l.google.com:19302"])
        client = RTCClient(iceServers: [iceServer, iceServere])
        client.delegate = self
        client.startConnection()
    }
    
    @IBAction func writeMessageButtonPressed(_ sender: UIButton) {
        socketSignal.send(nonce: textField.text!)
    }

}

extension MainViewController: RTCClientDelegate {
    
    // FIRST
    func rtcClient(client: RTCClient, didCreateLocalCapturer capturer: RTCCameraVideoCapturer) {
        print("Did create local capturer")
        let settingsModel = RTCCapturerSettingsModel()
        captureController = RTCCapturer.init(withCapturer: capturer, settingsModel: settingsModel)
        captureController.startCapture()
    }
    
    func rtcClient(client : RTCClient, didReceiveError error: Error) {
        // Error Received
        print("Error: \(error.localizedDescription)")
    }
    
    func rtcClient(client : RTCClient, didGenerateIceCandidate iceCandidate: RTCIceCandidate) {
        // iceCandidate generated, pass this to other user using any signal method your app uses
        print("Did generate ice candidates")
        client.addIceCandidate(iceCandidate: iceCandidate)
    }
    
    func rtcClient(client : RTCClient, startCallWithSdp sdp: String) {
        // SDP generated, pass this to other user using any signal method your app uses
        print("Start call!!!")
        //socketSignal.send(sdp: sdp)
    }
    
    // FIRST
    func rtcClient(client : RTCClient, didReceiveLocalVideoTrack localVideoTrack: RTCVideoTrack) {
        // Use localVideoTrack generated for rendering stream to remoteVideoView
        print("did Recieve local video track")
        localVideoTrack.add(localVideoView)
        self.localVideoTrack = localVideoTrack
    }
    
    func rtcClient(client : RTCClient, didReceiveRemoteVideoTrack remoteVideoTrack: RTCVideoTrack) {
        // Use remoteVideoTrack generated for rendering stream to remoteVideoView
        print("Did remote video track")
        remoteVideoTrack.add(remoteVideoView)
        self.remoteVideoTrack = remoteVideoTrack
    }
    
}
